## Terminando a batalha épica por ícones de sobreposição
###### Uma batalha épica está acontecendo no fundo do registro do meu PC com Windows. Os combatentes: Dropbox, Google Drive, OneDrive e as forças aliadas do TortoiseGit, TortoiseHg e TortoiseSVN. As armas: espaços em branco.

###### Créditos: https://cito.github.io/blog/overlay-icon-battle/

Requer [Python](https://www.python.org/) v3.6

Instale o virtualenv na instalação do python 3.6
```zsh
DIR_PYTHON36\Scripts>pip install virtualenv
```
No diretorio do projeto configure o virtual env com base no diretorio de instalação do python
```zsh
DIR_PYTHON36\Scripts\virtualenv.exe venv
```
Ative o venv
```zsh
D:\DIR_PROJETO>venv\Scripts\activate.bat
```
Para rodar
```zsh
(venv) D:\DIR_PROJETO>python overlay_icon_battle.py
```
